const express = require('express');
const app = express();
const PORT = process.env.port || 8080;
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');

app.use(
    express.urlencoded({
        extended: true,
    }),
);


app.use(morgan('dev'));

const authRouter = require('./routes/authRouter');
const boardRouter = require('./routes/boardRouter')
const userRouter = require('./routes/userRouter');
const cookieParser = require("cookie-parser");

app.use(express.json());
app.use(cookieParser());
app.use(cors({
    origin: ['http://localhost:4200'],
    credentials: true
}));

app.use('/api/auth', authRouter);
app.use('/api/boards', boardRouter);
app.use('/api/user', userRouter);

async function start() {
    await mongoose.connect('mongodb+srv://oleksandr_ch:salvator86@cluster0.pmlbz8i.mongodb.net/');
    app.listen(PORT, () => console.log(`Server started at port: ${PORT}`))
}

start();

app.use(errorHandler);

function errorHandler(err, req, res) {
    console.error(err);
    res.status(500).send({ message: 'Server error' });
}
