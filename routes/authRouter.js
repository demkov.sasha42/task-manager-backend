const express = require('express');
const router = express.Router();
const {userLogin, userRegister, getAuth, logOut} = require('../services/authService');
const upload = require("../middlewares/upload");

router.post('/login', userLogin);
router.post('/register', upload.single('image'), userRegister);
router.get('/authUser', getAuth);
router.post('/logOut', logOut);

module.exports = router;
