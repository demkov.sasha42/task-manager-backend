const express = require('express');
const { authMiddleware } = require("../middlewares/authMiddleware");
const upload = require('../middlewares/upload');
const {getUserInfo, editUserInfo, changePassword, deleteUser, setPhoto} = require("../services/userService");
const router = express.Router();

router.get('/', authMiddleware, getUserInfo);
router.patch('/', authMiddleware, editUserInfo);
router.patch('/password', authMiddleware, changePassword);
router.delete('/', authMiddleware, deleteUser);
router.patch('/setPhoto', authMiddleware, upload.single('image'), setPhoto)

module.exports = router;
