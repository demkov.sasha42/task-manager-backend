const express = require('express');
const { authMiddleware } = require("../middlewares/authMiddleware");
const router = express.Router();
const { getBoard, addBoard, editBoard, deleteBoard, getAllBoards, editBoardColor} = require('../services/boardService');
const { getTask, addTask, editTask, deleteTask, changePhotoTasks} = require('../services/taskService');
const upload = require("../middlewares/upload");
const {permissionMiddleware} = require("../middlewares/permissionMiddleware");

router.post('/', authMiddleware, addBoard);
router.get('/', authMiddleware, getAllBoards);
router.get('/:id', authMiddleware, permissionMiddleware, getBoard);
router.patch('/:id', authMiddleware, permissionMiddleware, editBoard);
router.patch('/:id/changeColor', authMiddleware, permissionMiddleware, editBoardColor);
router.delete('/:id', authMiddleware, permissionMiddleware, deleteBoard);

router.get('/:id/:idTask', authMiddleware, permissionMiddleware, getTask);
router.post('/:id', authMiddleware, permissionMiddleware, upload. single('image'), addTask);
router.patch('/:id/:idTask', authMiddleware, permissionMiddleware, editTask);
router.delete('/:id/:idTask', authMiddleware, permissionMiddleware, deleteTask);
router.patch('/:id/:idTask/changePhoto', authMiddleware, permissionMiddleware,
    upload.single('image'), changePhotoTasks);

module.exports = router;
