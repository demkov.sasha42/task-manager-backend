const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    username: {
        type: String,
            required: true,
            unique: true
    },
    image: {
        type: String,
    default: ''
    },
    password: {
        type: String,
            required: true,
    },
    createdDate: {
        type: Object
    }
}, { timestamps: true })

const User = mongoose.model('users', userSchema);

module.exports = {
    User,
};
