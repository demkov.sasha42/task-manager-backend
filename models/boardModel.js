const mongoose = require('mongoose');
const { statusTasks, borderColorTasks } = require('../data/shared')

const boardSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    backgroundColorNew: {
        type: String,
        required: true,
        enum: ['blue', 'red', 'green'],
        default: 'red'
    },
    backgroundColorProgress: {
        type: String,
        required: true,
        enum: ['blue', 'red', 'green'],
        default: 'blue'
    },
    backgroundColorDone: {
        type: String,
        required: true,
        enum: ['blue', 'red', 'green'],
        default: 'green'
    },
    tasks: [{
        name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        status: {
            type: String,
            default: 'NEW',
            enum: statusTasks,
            required: true
        },
        isArchived: {
            type: Boolean,
            default: false,
            required: true
        },
        image: {
            type: String,
            default: ''
        },
        borderColor: {
            type: String,
            default: 'blue',
            enum: borderColorTasks,
            required: true
        },
        createdAt: {
            type: Object,
            required: true
        }
    }, { timestamps: true }]
}, { timestamps: true });

const Boards = mongoose.model('boards', boardSchema);

module.exports = {
    Boards,
};
