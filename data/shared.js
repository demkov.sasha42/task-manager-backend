const statusTasks = ['NEW', 'IN PROGRESS', 'DONE'];
const borderColorTasks = ['blue', 'green', 'red'];

module.exports = {
    statusTasks,
    borderColorTasks
}
