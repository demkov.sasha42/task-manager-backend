const {Boards} = require("../models/boardModel");
const {User} = require("../models/userModel");
const bcrypt = require("bcryptjs");
const {deleteFile} = require("../FileSystem/fsController");

const findUserBoards = async (req) => {
    return Boards.find({userId: req.user.userId});
}

const findUserBoardsAndDelete = async (req) => {
    return Boards.deleteMany({userId: req.user.userId});
}

const findUserAndUpdateUsername = async (req, username) => {
    return User.findByIdAndUpdate({_id: req.user.userId }, { $set: { username } });
}

const findUserAndUpdatePhoto = async (req, image) => {
    return User.findByIdAndUpdate({_id: req.user.userId }, { $set: { image } });
}

const findUserOne = async (req) => {
    return User.findOne({ _id: req.user.userId });
}

const findUserAndUpdatePassword = async (req, password, newPassword) => {
    return User.findByIdAndUpdate({_id: req.user.userId}, { $set: { password: await bcrypt.hash(newPassword, 10) } });
}

const deleteUserData = async (req) => {
    await Boards.find({userId: req.user.userId})
        .then(boards => {
            boards.forEach(board => {
                board.tasks.forEach(task => {
                    deleteFile(task.image);
                })
            })
            })
    await User.findById({_id: req.user.userId})
        .then(user => {
            deleteFile(user.image);
        })
    return User.findByIdAndDelete(req.user.userId);
}

module.exports = {
    findUserBoards,
    findUserAndUpdateUsername,
    findUserOne,
    findUserAndUpdatePassword,
    deleteUserData,
    findUserBoardsAndDelete,
    findUserAndUpdatePhoto
}
