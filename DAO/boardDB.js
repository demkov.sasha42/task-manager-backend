const {Boards} = require("../models/boardModel");
const {deleteFile} = require("../FileSystem/fsController");

const editBoardData = async (req, name, description) => {
    return Boards.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId },
        { $set: { name, description}},
        {returnOriginal: false});
}

const deleteBoardData = async (req) => {
    await Boards.findOne({_id: req.params.id})
        .then(board => {
            board.tasks.forEach(task => {
                deleteFile(task.image);
            });
        });
    return Boards.findByIdAndDelete(req.params.id)
}

module.exports = {
    editBoardData,
    deleteBoardData
}
