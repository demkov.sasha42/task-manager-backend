const {Boards} = require("../models/boardModel");

const createTaskData = async (_id, userId, name, description, status, isArchived, image, borderColor, req) => {
    await Boards.updateOne({
        _id,
        createdBy: userId
    }, {
        '$push': {
            tasks: {
                name,
                description,
                status,
                isArchived,
                image,
                borderColor,
                createdAt: req._startTime
            }
        }
    }, {
        runValidators: true
    })
}

const findBoardData = async (req) => {
    return Boards.findById(req.params.id)
}

const addTaskToBoard = async (req, tasks) => {
    return Boards.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { tasks } })
}

const editTaskData = async (idTask, name, description, status, isArchived, borderColor) => {
    await Boards.updateOne({'tasks._id': idTask}, {'$set': {
            'tasks.$.name': name,
            'tasks.$.description': description,
            'tasks.$.status': status,
            'tasks.$.isArchived': isArchived,
            'tasks.$.borderColor': borderColor
        }}, {
        runValidators: true
    })
}

const deleteTaskData = async (_id, userId, idTask) => {
    return Boards.findOneAndUpdate({
        _id
    }, {
        $pull: {
            tasks: {
                _id: idTask
            }
        }
    })
}

const changePhotoTaskData = async (idTask, image) => {
    await Boards.updateOne({'tasks._id': idTask}, {'$set': {
            'tasks.$.image': image,
        }})
}

module.exports = {
    findBoardData,
    editTaskData,
    addTaskToBoard,
    deleteTaskData,
    changePhotoTaskData,
    createTaskData
}
