const {Boards} = require('../models/boardModel');
const {findBoardData} = require("../DAO/tasksDB");
const {editBoardData, deleteBoardData} = require("../DAO/boardDB");

const addBoard = (req, res) => {
    try {
        const {name, description} = req.body;
        if (name && description) {
            const board = new Boards({
                name,
                description,
                userId: req.user.userId,
            });
            board.save().then(() => {
                res.json(board);
            });
        } else {
            res.status(400).json({
                "message": "string"
            });
        }
    } catch (error) {
        res.status(500).json({
            "message": "string"
        });
    }
}

const getAllBoards = async (req, res) => {
    try {
        return Boards.find({userId: req.user.userId})
            .then(data => {
                res.status(200).json(data)
            })
    } catch (error) {
        res.status(501).json({message: error.message});
    }
}

const getBoard = async (req, res) => {
    try {
        if (req.url) {
            await findBoardData(req)
                .then((boards) => {
                    res.json({
                        board: {
                            _id: boards._id,
                            userId: boards.userId,
                            description: boards.description,
                            name: boards.name,
                            createdDate: boards.createdDate,
                            tasks: boards.tasks
                        }
                    });
                });
        } else {
            res.status(400).json({
                "message": "string"
            });
        }
    } catch (error) {
        res.status(400).json({
            "message": "string"
        });
    }
}

const editBoard = async (req, res) => {
    try {
        const {name, description} = req.body;
        await editBoardData(req, name, description)
            .then(board => {
                res.json(board);
            });
    } catch (err) {
        res.status(400).json({
            message: 'failed to update board'
        })
    }
};

const editBoardColor = async (req, res) => {
    try {
        const {column, inputColor} = req.body;
        console.log(req.body)
        // if (column = backgroundColorNew) {
        Boards.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId },
            { $set: { [column]: inputColor }},
            {returnOriginal: false}).then(board => {
                res.status(200).json(board)
        })
        // } else if (backgroundColorProgress) {
        //     Boards.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId },
        //         { $set: { 'backgroundColorProgress': backgroundColorProgress }},
        //         {returnOriginal: false}).then(board => {
        //             res.status(200).json(board)
        //     })
        // // } else if (backgroundColorDone) {
        //     Boards.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId },
        //         { $set: { 'backgroundColorDone': backgroundColorDone }},
        //         {returnOriginal: false}).then(board => {
        //             res.status(200).json(board)
        //     })
        // } else {
        //     res.status(400).json({
        //         message: 'failed to update board'
        //     })
        // }
    } catch (error) {
        res.status(501).json({
            message: 'Server error'
        })
    }
}

const deleteBoard = async (req, res) => {
    try {
        if (req.url) {
            await deleteBoardData(req)
                .then(() => {
                    res.json(req.url);
                });
        } else {
            res.status(400).json({
                "message": "Failed to delete board"
            });
        }
    } catch (error) {
        res.status(500).json({
            "message": "string"
        });
    }
}

module.exports = {
    getBoard,
    addBoard,
    editBoard,
    deleteBoard,
    getAllBoards,
    editBoardColor
}
