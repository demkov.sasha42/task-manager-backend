const {User} = require('../models/userModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const userRegister = async (req, res, next) => {
    try {
        const { username, password } = req.body;

        if(await User.findOne({ username: req.body.username }).username !== req.body.username && password) {
            const user = new User({
                username,
                password: await bcrypt.hash(password, 10),
                image: req.file ? req.file.path : '',
                createDate: req._startTime
            });

            user.save()
                .then(() => res.status(200).json({
                    "message": "Success registration!"
                }))
                .catch(err => {
                    next(err);
                });
        } else {
            res.status(400).json({
                "message": "Something wrong :("
            });
        }
    } catch (error) {
        res.status(500).json({
            "message": "Bad connection"
        });
    }
}

const userLogin = async (req, res, next) => {
    try {
        const user = await User.findOne({ username: req.body.username });
        if (!user) {
            return res.status(404).json({
                "message": "User not found"
            });
        }
        if (!await bcrypt.compare(String(req.body.password), String(user.password))) {
            return res.status(405).json({
                "message": "You printed incorrect password"
            });
        }
        const payload = { username: user.username, userId: user._id };
        const jwtToken = jwt.sign(payload, 'secret-jwt-key');
        const day = 24 * 60 * 60 * 1000;
        res.cookie('jwt', jwtToken, { httpOnly: true, maxAge: day });
        const { password, ...data } = await user.toJSON();
        return res.json(data);
    } catch (error) {
        return res.json({status: false, message: error.message});
    }
}

const getAuth = async (req, res) => {
    try {
        const cookie = req.cookies['jwt'];
        const claims = jwt.verify(cookie, 'secret-jwt-key');
        if (!claims) {
            return res.status(401).send({ message: 'Unauthenticated' });
        }
        const user = await User.findOne({ _id: claims.userId });
        const { password, ...data } = await user.toJSON();
        res.send(data);
    } catch(e) {
        return res.status(402).send({ message: 'Unauthenticated' });
    }
}

const logOut = async (req, res) => {
    try {
        console.log(req.cookies)
        res.cookie('jwt', '', { maxAge: 0 });
        res.send({ message: 'Success', jwt: req.cookies });
    } catch (e) {
        res.status(500).send({ message: e.message });
    }
}

module.exports = {
    userLogin,
    userRegister,
    getAuth,
    logOut
}
