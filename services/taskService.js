const { findBoardData, editTaskData, deleteTaskData, changePhotoTaskData, createTaskData} = require("../DAO/tasksDB");
const {deleteFile} = require("../FileSystem/fsController");
const {Boards} = require("../models/boardModel");

const addTask = async (req, res) => {
    try {
        const { name, description, isArchived, status, borderColor } = req.body;
        const image = req.file ? req.file.path : '';
        const userId = req.user.userId
        const _id = req.params.id;
        await createTaskData(_id, userId, name, description, status, isArchived, image, borderColor, req)
            .then(() => {
                Boards.find({_id: _id}).then(board => {
                    res.json(...board);
                })
            });
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
}

const getTask = async (req, res) => {
    try {
        await findBoardData(req)
            .then((boards) => {
                res.status(200).json({
                    task: boards.tasks.filter(task => task._id.toString() === `${req.params.idTask}`)
                });
            });
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
}

const editTask = async (req, res) => {
    try {
        const {id, idTask} = req.params;
        const {name, description, status, isArchived, borderColor} = req.body;
        await editTaskData(idTask, name, description, status, isArchived, borderColor);
        await Boards.findOne({_id: id}).then(board => {
            res.status(200).json(board)
        })
    } catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
}

const deleteTask = async (req, res) => {
    try {
        const { userId } = req.user.userId;
        const { id, idTask } = req.params;
        await deleteTaskData(id, userId, idTask)
        await Boards.findOne({_id: id}).then(board => {
            res.status(200).json(board);
        })
    } catch (err) {
        res.status(400).json({
            message: "Task didn't delete"
        });
    }
}

const changePhotoTasks = async (req, res) => {
    try {
        const {idTask} = req.params;
        const image = req.file ? req.file.path : '';
        let oldPath;

        await findBoardData(req)
            .then(board => {
                const task = board.tasks.find(task => task._id.toString() === idTask);
                oldPath = task.image;
            })

        await deleteFile(oldPath);

        await changePhotoTaskData(idTask, image)
            .then(() => {
                res.status(200).json({
                    message: 'Photo of task was updated'
                })
            })
    } catch (err) {
        res.status(400).json({
            message: 'Received wrong data'
        })
    }
}

module.exports = {
    getTask,
    addTask,
    editTask,
    deleteTask,
    changePhotoTasks
}
