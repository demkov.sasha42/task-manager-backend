const {findUserBoards, findUserAndUpdateUsername, findUserAndUpdatePassword, findUserOne, deleteUserData,
    findUserBoardsAndDelete, findUserAndUpdatePhoto
} = require("../DAO/userDB");
const bcrypt = require('bcryptjs');
const {deleteFile} = require("../FileSystem/fsController");

const getUserInfo = async (req, res) => {
    try {
        await findUserBoards(req)
            .then(usersBoards => {
                res.status(200).json({
                    message: {
                        username: req.user.username,
                        boards: usersBoards
                    }
                });
            });
    } catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
}

const editUserInfo = async (req, res) => {
    try {
        const { username } = req.body;
        await findUserAndUpdateUsername(req, username).then(() => {
            res.status(200).json({username: username});
        });
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
}

const changePassword = async (req, res) => {
    try {
        const { oldPassword, newPassword } = req.body;
        let userPassword;
        await findUserOne(req)
            .then((result) => userPassword = result.password);
        if(await bcrypt.compare(String(oldPassword), String(userPassword))) {
            await findUserAndUpdatePassword(req, userPassword, newPassword)
                .then(() => {
                    res.json({ message: 'Password was updated' });
                });
        }
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
}

const deleteUser = async (req, res) => {
    try {
        await deleteUserData(req)
            .then(async () => {
                await findUserBoardsAndDelete(req);
                res.status(200).json({
                    message: 'User was deleted'
                })
                }
            )
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
}

const setPhoto = async (req, res) => {
    try {
        let oldPath;
        const image = req.file ? req.file.path : '';
        await findUserOne(req)
            .then(data => {
                oldPath = data.image
            });

        await deleteFile(oldPath);

        await findUserAndUpdatePhoto(req, image)
            .then(() => {
                res.status(200).json({
                    message: 'Profile photo was updated'
                })
            })
    } catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
}

module.exports = {
    getUserInfo,
    editUserInfo,
    changePassword,
    deleteUser,
    setPhoto
}
