const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
    const jwtToken = req.cookies['jwt'];

    if (!jwtToken) {
        return res.status(401).json({ 'message': 'Please, provide authorization header' });
    }

    if (!jwtToken) {
        return res.status(401).json({ 'message': 'Please, include token to request' });
    }

    try {
        const tokenPayload = jwt.verify(jwtToken, 'secret-jwt-key');
        req.user = {
            userId: tokenPayload.userId,
            username: tokenPayload.username,
        }
        next();
    } catch (err) {
        return res.status(401).json({message: err.message});
    }
}

module.exports = {
    authMiddleware
}
