const {findBoardData} = require("../DAO/tasksDB");

const permissionMiddleware = async (req, res, next) => {
    try {
        await findBoardData(req).then(board => {
            if (req.user.userId === board.userId.toString()) {
                next();
            } else {
                res.status(401).json({message: 'You have no permission for this'})
            }
        })
    } catch (e) {
        res.status(500).json({
            message: 'Server error'
        })
    }
}

module.exports = {
    permissionMiddleware
}
